from matplotlib import pyplot
import matplotlib.patches as mpatches
from random import randint
import math
from typing import List

from graph import Graph, create_graph
import graph.random as random_graph
from graph.utils import connected_components, is_resilient
from performance import execution_time


@execution_time
def parse_graph(dataset_path: str) -> Graph:
    graph = create_graph(1476, 3132, Graph.Type.NOT_ORIENTED)
    node_map = dict()
    with open(dataset_path) as file:
        for line in file.readlines():
            if line[0] == '#':
                continue
            nums = line[:-1].split('\t')
            if len(nums) != 2:
                raise Exception("Unsupported input format.")
            if nums[0] != nums[1]:
                tail = nums[0]
                head = nums[1]
            else:
                continue

            tail_node = node_map.get(tail)  # O(n) n = number of nodes in data set.
            if tail_node is None:
                tail_node = graph.add_node(tail)
                node_map[tail] = tail_node

            head_node = node_map.get(head)
            if head_node is None:
                head_node = graph.add_node(head)
                node_map[head] = head_node
            if not graph.are_connected(tail_node, head_node):
                graph.add_edge(tail_node, head_node)
    return graph


@execution_time
def attack_simulation(graph: Graph):
    res = []
    while graph.nodes_count() > 0:
        res.append(max(map(lambda x: len(x), connected_components(graph))))
        graph.remove_node(graph[randint(0, graph.nodes_count()-1)])
    return res


def main():
    real_graph = parse_graph('dataset.txt')

    n = real_graph.nodes_count()
    p = real_graph.edge_count() / (n*(n-1))
    m = 2

    print("n = {0}".format(n))
    print("p = {0}".format(p))
    print("m = {0}".format(m))

    er_g = random_graph.er_graph(n, p, Graph.Type.NOT_ORIENTED)
    upa_g = random_graph.upa_graph(n, m, 'list')

    print("===Rete===\n-nodi: {0}\n-archi: {1}".format(real_graph.nodes_count(), real_graph.edge_count()))
    print("===ER===\n-nodi: {0}\n-archi: {1}".format(er_g.nodes_count(), er_g.edge_count()))
    print("===UPA===\n-nodi: {0}\n-archi: {1}".format(upa_g.nodes_count(), upa_g.edge_count()))

    real_graph_attack = attack_simulation(real_graph)
    er_graph_attack = attack_simulation(er_g)
    upa_graph_attack = attack_simulation(upa_g)

    is_real_resilient = is_resilient(n, real_graph_attack)
    is_er_resilient = is_resilient(n, er_graph_attack)
    is_upa_resilient = is_resilient(n, upa_graph_attack)

    print("Resilienza rete: {0}".format(is_real_resilient))
    print("Resilienza er: {0}".format(is_er_resilient))
    print("Resilienza upa: {0}".format(is_upa_resilient))

    resilient_index = math.floor(n * 0.2)

    pyplot.scatter(range(len(real_graph_attack)), real_graph_attack, s=1, color='r')
    pyplot.annotate(
        "Valore:{0}\n{1}".format(real_graph_attack[resilient_index], "Resiliente" if is_real_resilient else "Non resiliente"),
        xy=(resilient_index, real_graph_attack[resilient_index]), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

    pyplot.scatter(range(len(er_graph_attack)), er_graph_attack, s=1, color='g')
    pyplot.annotate(
        "Valore:{0}\n{1}".format(er_graph_attack[resilient_index], "Resiliente" if is_er_resilient else "Non resiliente"),
        xy=(resilient_index, er_graph_attack[resilient_index]), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

    pyplot.scatter(range(len(upa_graph_attack)), upa_graph_attack, s=1, color='b')
    pyplot.annotate(
        "Valore:{0}\n{1}".format(upa_graph_attack[resilient_index], "Resiliente" if is_upa_resilient else "Non resiliente"),
        xy=(resilient_index, upa_graph_attack[resilient_index]), xytext=(50, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

    # pyplot.plot([math.floor(n*.2), math.floor(n*.2)], [0, 2000], color='k', lw=1)
    # pyplot.plot([0, 3000], [math.floor(n*.75), math.floor(n*.75)], color='k', lw=1)
    pyplot.xlabel("Nodi rimossi")
    pyplot.ylabel("Dimensione massima componente connessa")
    red_patch = mpatches.Patch(color='r', label='Rete')
    green_patch = mpatches.Patch(color='g', label='Grafo ER p: {0}'.format(p))
    blue_patch = mpatches.Patch(color='b', label='Grafo UPA m: {0}'.format(m))
    pyplot.legend(handles=[red_patch, green_patch, blue_patch])
    pyplot.grid(True)
    pyplot.show()


if __name__ == "__main__":
    main()
