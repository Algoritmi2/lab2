from abc import ABC, abstractmethod
from enum import Enum
from typing import List


class Node:

    """
    Class that represents a graph node.
    """

    def __init__(self, index: int, value: object = None):
        self._index = index
        self._value = value

    def _get_index(self) -> int:
        return self._index

    def _get_value(self) -> object:
        return self._value

    def _set_value(self, value: object):
        self._value = value

    index = property(_get_index)

    value = property(_get_value, _set_value)


class Edge:

    """
    Class that represents an oriented edge between two nodes.
    v(Tail) -----> u(Head)
    """

    def __init__(self, tail: Node, head: Node):
        """
        Constructor
        :param tail: The edge tail.
        :param head: The edge head.
        """
        self._tail = tail
        self._head = head

    def _get_tail(self):
        return self._tail

    def _get_head(self):
        return self._head

    tail = property(_get_tail)

    head = property(_get_head)


class Graph(ABC):

    class Type(Enum):
        ORIENTED = 0
        NOT_ORIENTED = 1

    """
    Abstract class that represents a graph
    """
    @abstractmethod
    def add_node(self, value: object = None) -> Node:
        """
        Adds a node to the graph.
        :param value: A value associated to the added node.
        :return: Returns the added node.
        """
        pass

    def remove_node(self, node: Node):
        """
        Removes a node from the graph
        :param node: The node to remove.
        """
        pass

    def neighbors(self, node: Node) -> List[Node]:
        """
        Gets the list of neighbors of a node.
        :param node: The node of interest.
        :return: Returns the list of neighbors of the provided node.
        """

    @abstractmethod
    def get_nodes(self) -> List[Node]:
        """
        Gets the list off all the nodes in the graph.
        :return: Returns a List holding all the nodes inside the graph.
        """
        pass

    @abstractmethod
    def nodes_count(self) -> int:
        """
        Gets the number of nodes.
        :return: Returns the number of nodes in the graph.
        """
        pass

    @abstractmethod
    def add_edge(self, v: Node, u: Node):
        """
        Adds an edge between two node.
        :param v: the source node.
        :param u: the destination node.
        :return: Returns the current graph instance to provide a fluent API.
        """
        pass

    def edge_count(self) -> int:
        """
        Gets the number of edges in the graph
        :return:
        """
        pass

    @abstractmethod
    def are_connected(self, v: Node, u: Node) -> bool:
        """
        Tels if two nodes are connected with at least an edge.
        :param v: The first node.
        :param u: The second node.
        :return: Returns true if exist an edge between `u` and `v`.
        """
        pass

    @abstractmethod
    def in_degree(self, v: Node) -> int:
        """
        Gets the in degree of a node.
        :param v: The node of interest.
        :return: Returns the in degree of the requested node.
        """
        pass

    @abstractmethod
    def out_degree(self, v: Node) -> int:
        """
        Gets the out degree of a node.
        :param v: The node of interest.
        :return: Returns the out degree of the requested node.
        """
        pass

    def __iter__(self):
        return iter(self.get_nodes())

    def __getitem__(self, item):
        return self.get_nodes()[item]
