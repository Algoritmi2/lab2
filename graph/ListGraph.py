from typing import List

from collections import deque

from graph.Graph import Graph, Node


class ListGraph(Graph):

    """
    Implementation of the graph interface, that represents a graph using an adjacencies list.
    """

    def __init__(self, capacity: int, graph_type: Graph.Type = Graph.Type.ORIENTED):
        """
        :param capacity: The initial nodes capacity of the graph.
        :param graph_type: The graph type, can be Oriented(Type.ORIENTED) or Not oriented(Type.NOT_ORIENTED).
        """
        self._type = graph_type
        self._in_degree_list = [0] * capacity
        self._nodes = {}
        self._adjacencies_list = [[] for x in range(capacity)]
        self._removed_indices = deque()
        self._capacity = capacity

    def add_node(self, value: object = None):
        new_index = -1
        if len(self._removed_indices) > 0:
            new_index = self._removed_indices.popleft()
            self._nodes[new_index] = Node
        else:
            new_index = len(self._nodes)
        v = Node(new_index, value)
        if self._capacity <= new_index:
            self._capacity += 1
            self._adjacencies_list.append([])
            self._in_degree_list.append(0)
        self._nodes[new_index] = v
        self._adjacencies_list[new_index] = []
        self._in_degree_list[new_index] = 0
        return v

    def remove_node(self, node: Node):
        if node.index in self._nodes:
            removed = self._nodes.pop(node.index)
            self._update_adjacencies_list(removed)
            self._in_degree_list[node.index] = 0
            self._removed_indices.append(node.index)

    def neighbors(self, node: Node) -> List[Node]:
        return self._adjacencies_list[node.index]

    def get_nodes(self) -> List[Node]:
        return list(self._nodes.values())

    def nodes_count(self) -> int:
        return len(self._nodes)

    def out_degree(self, v: Node) -> int:
        return len(self._adjacencies_list[v.index])

    def in_degree(self, v: Node) -> int:
        return self._in_degree_list[v.index]

    def add_edge(self, tail: Node, head: Node):
        self._adjacencies_list[tail.index].append(self._nodes[head.index])
        self._in_degree_list[head.index] += 1
        if self._type == Graph.Type.NOT_ORIENTED:
            self._adjacencies_list[head.index].append(self._nodes[tail.index])
            self._in_degree_list[tail.index] += 1
        return self

    def edge_count(self) -> int:
        value = 0
        if self._type == Graph.Type.ORIENTED:
            value = sum(self._in_degree_list)
        else:
            value = sum(self._in_degree_list) / 2
        return int(value)

    def are_connected(self, v: Node, u: Node) -> bool:
        connected = False
        adjacencies_nodes = self._adjacencies_list[v.index]
        if adjacencies_nodes:
            for i in range(len(adjacencies_nodes)):
                if adjacencies_nodes[i].index == u.index:
                    connected = True
                    break
        return connected

    def _update_adjacencies_list(self, removed_node: Node):
        for n in self._adjacencies_list[removed_node.index]:
            self._in_degree_list[n.index] -= 1
        self._adjacencies_list[removed_node.index] = []
        for n in self._nodes.values():
            self._adjacencies_list[n.index] = list(filter(
                lambda x: x.index != removed_node.index,
                self._adjacencies_list[n.index]))
