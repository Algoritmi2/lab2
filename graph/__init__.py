from .Graph import Graph, Node, Edge
from .ListGraph import ListGraph
from .MatrixGraph import MatrixGraph


def create_graph(nodes_count: int, edge_count: int, graph_type: Graph.Type = Graph.Type.ORIENTED) -> Graph:

    """
    Create the best memory efficient graph representation.
    :param nodes_count: The number of nodes of the graph.
    :param edge_count: The number of edges in the graph.
    :param graph_type: The type of the graph.
    :return: Returns the best memory efficient graph.
    """

    edge_value = edge_count if graph_type == Graph.Type.ORIENTED else edge_count * 2
    graph = None
    if edge_value >= nodes_count*nodes_count:
        graph = create_matrix_graph(nodes_count, graph_type)
    else:
        graph = create_list_graph(nodes_count, graph_type)

    return graph


def create_list_graph(nodes_capacity: int, graph_type: Graph.Type) -> Graph:
    """
    Creates a graph represented using an adjacencies list.
    :param nodes_capacity: The initial nodes capacity of the graph.
    :param graph_type: The type of the graph.
    :return: Returns a graph that is represented using an adjacencies list.
    """
    return ListGraph(nodes_capacity, graph_type)


def create_matrix_graph(nodes_capacity: int, graph_type: Graph.Type) -> Graph:
    """
    Creates a graph represented using an adjacencies matrix.
    :param nodes_capacity: The initial nodes capacity of the graph.
    :param graph_type: The type of the graph.
    :return: Returns a graph that is represented using an adjacencies matrix.
    """
    return MatrixGraph(nodes_capacity, graph_type)

