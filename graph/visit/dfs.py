from typing import Dict, List

from graph import Graph, Node


def dfs_visit(graph: Graph, u: Node, color: Dict[int, str], visited: List[Node]):
    """
    visit all the  node path
    :param graph:
    :param u:
    :param color:
    :param visited:
    :return:
    """
    color[u.index] = 'g'
    visited.append(u)
    for v in graph.neighbors(u):
        if color[v.index] == 'w':
            visited = dfs_visit(graph, v, color, visited)
    color[u.index] = 'b'
    return visited
