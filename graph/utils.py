from typing import List

import math

from graph import Graph, Node
from graph.visit import dfs_visit


def in_deg_distribuition(graph: Graph) -> List[float]:

    indeg_occ_map = dict()
    for v in graph:
        in_deg = graph.in_degree(v)
        if in_deg in indeg_occ_map:
            indeg_occ_map[in_deg] += 1
        else:
            indeg_occ_map[in_deg] = 1

    values = [0] * (graph.nodes_count())
    for i in range(graph.nodes_count()):
        if i in indeg_occ_map:
            values[i] = indeg_occ_map[i]/graph.nodes_count()

    return values


def connected_components(graph: Graph):
    """
    Counts the number of connected components in graph
    :param graph:
    :return:
    """
    color = {}
    for n in graph:
        color[n.index] = 'w'

    CC = []
    for v in graph:
        if color[v.index] == 'w':
            comp = dfs_visit(graph, v, color, [])
            CC.append(comp)
    return CC


def get_max_out_degree_node(graph: Graph) -> Node:
    deg = -1
    node = None
    for v in graph:
        out_deg = graph.out_degree(v)
        if out_deg > deg:
            deg = out_deg
            node = v
    return node


def is_resilient(node_count: int, attack_result: List[int]) -> bool:
    removed_node_index = int(math.floor(node_count * 0.2))
    min_resilient_value = math.floor(node_count * 0.75)
    return attack_result[removed_node_index] >= min_resilient_value

