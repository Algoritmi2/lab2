from graph import Graph, ListGraph, MatrixGraph
import random


class UPATrial:

    def __init__(self, m: int):
        self._num_nodes = m
        self._node_numbers = []
        for i in range(m):
            self._node_numbers.extend([i for j in range(m)])

    def run_trial(self, m):
        v = []
        for i in range(m):
            u = random.choice(self._node_numbers)
            v.append(u)
        new_node = self._num_nodes

        # Dato che ora il grafo è non orientato il grado di connesione del nuovo nodo è
        # 1 + il numero di nodi che verranno connessi ad esso, percio aggiungo m + 1
        # volte il nuovo nodo.
        for i in range(m):
            self._node_numbers.append(new_node)

        self._node_numbers.extend(v)
        self._num_nodes += 1
        return v


def upa_graph(n: int, m: int, representation: str = 'matrix') -> Graph:
    """
    Generates a random oriented graph using the DPA algorithm.
    :param n: The number of nodes in the graph.
    :param m:
    :param representation: The
    :return: The internal representation of the generated graph,
             `list` to use an adjacencies list, `matrix` to use adjacencies matrix.
    """

    if 1 > m > n:
        raise ValueError("The value m must be in [1, n]")

    graph = None  # type: Graph
    if representation == 'list':
        graph = ListGraph(n, Graph.Type.NOT_ORIENTED)
    elif representation == 'matrix':
        graph = MatrixGraph(n, Graph.Type.NOT_ORIENTED)
    else:
        raise ValueError("Unsupported value: {} for the representation param.".format(representation))

    # Adds m nodes in the graph
    for i in range(m):
        graph.add_node()
    # Make the graph completed
    for u in graph:
        for v in graph:
            if u.index == v.index:
                continue
            graph.add_edge(u, v)

    upa_trial = UPATrial(m)
    for u in range(m, n):
        v = upa_trial.run_trial(m)
        node_u = graph.add_node()
        for i in v:
            node_v = graph[i]
            graph.add_edge(node_u, node_v)

    return graph
