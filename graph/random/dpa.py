from graph import Graph, ListGraph, MatrixGraph
import random


class DPATrial:

    def __init__(self, m: int):
        self._num_nodes = m
        self._node_numbers = []
        for i in range(m):
            self._node_numbers.extend([i for j in range(m)])

    def run_trial(self, m):
        V = []
        for i in range(m):
            u = random.choice(self._node_numbers)
            V.append(u)
        self._node_numbers.append(self._num_nodes)
        self._node_numbers.extend(V)
        self._num_nodes += 1
        return V


def dpa_graph(n: int, m: int, representation: str = 'matrix') -> Graph:
    """
    Generates a random graph using the DPA algorithm.
    :param n: The number of nodes in the graph.
    :param m:
    :param representation: The
    :return: The internal representation of the generated graph,
             `list` to use an adjacencies list, `matrix` to use adjacencies matrix.
    """

    if 1 > m > n:
        raise ValueError("The value m must be in [1, n]")

    graph = None  # type: Graph
    if representation == 'list':
        graph = ListGraph(n, Graph.Type.ORIENTED)
    elif representation == 'matrix':
        graph = MatrixGraph(n, Graph.Type.ORIENTED)
    else:
        raise ValueError("Unsupported value: {} for the representation param.".format(representation))

    # Adds m nodes in the graph
    for i in range(m):
        graph.add_node()
    # Make the graph completed
    for u in graph:
        for v in graph:
            if u.index == v.index:
                continue
            graph.add_edge(v, u)
            graph.add_edge(u, v)

    dpatTrial = DPATrial(m)
    for u in range(m, n):
        v = dpatTrial.run_trial(m)
        node_u = graph.add_node()
        for i in v:
            node_v = graph.get_node_by_index(i)
            graph.add_edge(node_u, node_v)

    return graph
