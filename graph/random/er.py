import numpy as np
from graph import MatrixGraph, ListGraph, Graph


def er_graph(n: int, p: float, g_type: Graph.Type = Graph.Type.ORIENTED, representation: str = 'list'):
    """
    Creates a random graph using the ER algorithm.
    :param n: The number of nodes in the graph.
    :param p: The probability that 2 nodes are connected with a an edge, must be in [0, 1)
    :param g_type: The type of graph, can be Graph.Type.ORIENTED or Graph.Type.NOT_ORIENTED
    :param representation: The internal representation of the generated graph,
                           `list` to use an adjacencies list, `matrix` to use adjacencies matrix.
    :return:
    """

    if g_type != Graph.Type.ORIENTED and g_type != Graph.Type.NOT_ORIENTED:
        raise ValueError("Unsupported graph type {}".format(g_type))

    if p >= 1 or p < 0:
        raise ValueError("Unsupported value for the `p` param, the value must be in [0,1)")

    graph = None
    if representation == 'list':
        graph = ListGraph(n, Graph.Type.ORIENTED)
    elif representation == 'matrix':
        graph = MatrixGraph(n, Graph.Type.ORIENTED)
    else:
        raise ValueError("Unsupported value: {} for the representation param.".format(representation))

    for i in range(n):
        graph.add_node()

    for u in graph:
        for v in graph:
            if v.index == u.index or (g_type == Graph.Type.NOT_ORIENTED and graph.are_connected(u, v)):
                continue
            a = np.random.sample()
            if a < p:
                graph.add_edge(u, v)
    return graph
