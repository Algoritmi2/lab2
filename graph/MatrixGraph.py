from typing import List
from collections import deque

from graph.Graph import Graph, Node


class MatrixGraph(Graph):

    """
    Implementation of the graph interface, that represents a graph using an adjacencies matrix.
    """

    def __init__(self, capacity: int, graph_type: Graph.Type = Graph.Type.ORIENTED):
        """
        :param capacity: The initial nodes capacity of the graph.
        :param graph_type: The graph type, can be Oriented(Type.ORIENTED) or Not oriented(Type.NOT_ORIENTED).
        """
        self._type = graph_type
        self._in_degrees = [0] * capacity
        self._out_degrees = [0] * capacity
        self._nodes = {}
        self._removed_indices = deque()
        self._capacity = capacity
        if graph_type == Graph.Type.ORIENTED:
            self._adjacencies_matrix = [[0 for x in range(capacity)] for y in range(capacity)]
        else:
            self._adjacencies_matrix = [None] * capacity
            for i in range(capacity):
                self._adjacencies_matrix[i] = [0] * i

    def add_node(self, value: object = None):
        new_index = len(self._nodes)
        if len(self._removed_indices) > 0:
            new_index = self._removed_indices.popleft()
        v = Node(new_index, value)
        self._nodes[new_index] = v
        if new_index >= self._capacity:
            self._capacity += 1
            self._in_degrees.append(0)
            self._out_degrees.append(0)
            self._adjacencies_matrix.append([0] * self._capacity)
            for i in range(len(self._nodes)):
                self._adjacencies_matrix[i].append(0)
        else:
            self._in_degrees[new_index] = 0
            self._out_degrees[new_index] = 0
        return v

    def remove_node(self, node: Node):
        if node.index in self._nodes:
            removed = self._nodes.pop(node.index)
            self._update_degree_values(removed)
            self._out_degrees[removed.index] = 0
            self._in_degrees[removed.index] = 0
            self._removed_indices.append(removed.index)

    def neighbors(self, node: Node) -> List[Node]:
        neighbors = []
        for n in self._nodes.values():
            if n.index == node.index:
                continue
            if self._get_adjacencies_value(node, n) > 0:
                neighbors.append(n)
        return neighbors

    def get_nodes(self) -> List[Node]:
        return list(self._nodes.values())

    def nodes_count(self) -> int:
        return len(self._nodes)

    def out_degree(self, v: Node) -> int:
        return self._out_degrees[v.index]

    def in_degree(self, v: Node) -> int:
        return self._in_degrees[v.index]

    def add_edge(self, tail: Node, head: Node):
        self._in_degrees[head.index] += 1
        self._out_degrees[tail.index] += 1
        if self._type == Graph.Type.NOT_ORIENTED:
            self._in_degrees[tail.index] += 1
            self._out_degrees[head.index] += 1

        self._increment_adjacencies_value(tail, head, 1)
        return self

    def edge_count(self) -> int:
        value = 0
        indeg_values = self._in_degrees
        if self._type == Graph.Type.ORIENTED:
            value = sum(indeg_values)
        else:
            value = sum(indeg_values) / 2
        return int(value)

    def are_connected(self, v: Node, u: Node) -> bool:
        return self._get_adjacencies_value(v, u) > 0

    def _update_degree_values(self, removed: Node):
        for node in self._nodes.values():
            self._out_degrees[node.index] -= self._get_adjacencies_value(node, removed)
            self._in_degrees[node.index] -= self._get_adjacencies_value(removed, node)
            self._set_adjacencies_value(node, removed, 0)
            self._set_adjacencies_value(removed, node, 0)

    def _set_adjacencies_value(self, tail: Node, head: Node, value: int):
        first, second = (tail, head) if self._type == Graph.Type.ORIENTED or tail.index >= head.index else (head, tail)
        self._adjacencies_matrix[first.index][second.index] = value

    def _get_adjacencies_value(self, tail: Node, head: Node) -> int:
        first, second = (tail, head) if self._type == Graph.Type.ORIENTED or tail.index >= head.index else (head, tail)
        return self._adjacencies_matrix[first.index][second.index]

    def _increment_adjacencies_value(self, tail: Node, head: Node, value: int):
        first, second = (tail, head) if self._type == Graph.Type.ORIENTED or tail.index >= head.index else (head, tail)
        self._adjacencies_matrix[first.index][second.index] += value