import unittest

from graph import Graph, create_matrix_graph


def demo_graph(type: Graph.Type) -> Graph:
    graph = create_matrix_graph(5, type)
    for x in range(5):
        graph.add_node()
    graph.add_edge(graph[0], graph[1])
    graph.add_edge(graph[0], graph[2])
    graph.add_edge(graph[1], graph[3])
    graph.add_edge(graph[1], graph[3])
    graph.add_edge(graph[2], graph[3])
    graph.add_edge(graph[3], graph[4])
    return graph


class TestMatrixGraph(unittest.TestCase):

    def test_oriented_degrees(self):
        graph = demo_graph(Graph.Type.ORIENTED)
        self.assertEqual(graph.in_degree(graph[0]), 0)
        self.assertEqual(graph.out_degree(graph[0]), 2)
        self.assertEqual(graph.in_degree(graph[1]), 1)
        self.assertEqual(graph.out_degree(graph[1]), 2)
        self.assertEqual(graph.in_degree(graph[2]), 1)
        self.assertEqual(graph.out_degree(graph[2]), 1)
        self.assertEqual(graph.in_degree(graph[3]), 3)
        self.assertEqual(graph.out_degree(graph[3]), 1)
        self.assertEqual(graph.in_degree(graph[4]), 1)
        self.assertEqual(graph.out_degree(graph[4]), 0)

    def test_not_oriented_degrees(self):
        graph = demo_graph(Graph.Type.NOT_ORIENTED)
        self.assertEqual(graph.in_degree(graph[0]), 2)
        self.assertEqual(graph.out_degree(graph[0]), 2)
        self.assertEqual(graph.in_degree(graph[1]), 3)
        self.assertEqual(graph.out_degree(graph[1]), 3)
        self.assertEqual(graph.in_degree(graph[2]), 2)
        self.assertEqual(graph.out_degree(graph[2]), 2)
        self.assertEqual(graph.in_degree(graph[3]), 4)
        self.assertEqual(graph.out_degree(graph[3]), 4)
        self.assertEqual(graph.in_degree(graph[4]), 1)
        self.assertEqual(graph.out_degree(graph[4]), 1)

    def test_oriented_remove(self):
        graph = demo_graph(Graph.Type.ORIENTED)
        graph.remove_node(graph[3])
        self.assertEqual(graph.nodes_count(), 4)
        self.assertEqual(len(graph.get_nodes()), 4)

        self.assertEqual(graph.out_degree(graph[1]), 0)
        self.assertEqual(graph.in_degree(graph[1]), 1)

        self.assertEqual(graph.out_degree(graph[2]), 0)
        self.assertEqual(graph.in_degree(graph[2]), 1)

        self.assertEqual(graph.out_degree(graph[3]), 0)
        self.assertEqual(graph.in_degree(graph[3]), 0)

    def test_not_oriented_remove(self):
        graph = demo_graph(Graph.Type.NOT_ORIENTED)
        graph.remove_node(graph[3])
        self.assertEqual(graph.nodes_count(), 4)
        self.assertEqual(len(graph.get_nodes()), 4)

        self.assertEqual(graph.out_degree(graph[1]), 1)
        self.assertEqual(graph.in_degree(graph[1]), 1)

        self.assertEqual(graph.out_degree(graph[2]), 1)
        self.assertEqual(graph.in_degree(graph[2]), 1)

        self.assertEqual(graph.out_degree(graph[3]), 0)
        self.assertEqual(graph.in_degree(graph[3]), 0)

    def test_oriented_neighbors(self):
        graph = demo_graph(Graph.Type.ORIENTED)
        neighbors = graph.neighbors(graph[0])
        self.assertAlmostEqual(len(neighbors), 2)
        graph.remove_node(graph[1])
        neighbors = graph.neighbors(graph[0])
        self.assertAlmostEqual(len(neighbors), 1)

    def test_not_oriented_neighbors(self):
        graph = demo_graph(Graph.Type.NOT_ORIENTED)
        neighbors = graph.neighbors(graph[0])
        self.assertAlmostEqual(len(neighbors), 2)
        graph.remove_node(graph[1])
        neighbors = graph.neighbors(graph[0])
        self.assertAlmostEqual(len(neighbors), 1)

    def test_not_oriented_edges(self):
        graph = demo_graph(Graph.Type.NOT_ORIENTED)
        self.assertEqual(graph.edge_count(), 6)

    def test_oriented_edges(self):
        graph = demo_graph(Graph.Type.ORIENTED)
        self.assertEqual(graph.edge_count(), 6)
